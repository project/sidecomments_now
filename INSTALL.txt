SIDECOMMENTS NOW INSTALLATION INSTRUCTIONS
------------------------------------------

This module requires the following modules:

 * Comment (core)
 * simplehtmldom API (https://www.drupal.org/project/simplehtmldom)
 * Libraries API (https://www.drupal.org/project/libraries)


 1. Unpack the SideComments Now folder and contents in the appropriate modules
    directory of your Drupal installation. (Example: sites/all/modules/contrib)

 2. Download the SideComments.js library from
    https://aroc.github.io/side-comments-demo/. It is important to use the 
    master version, located at 
    https://github.com/aroc/side-comments/archive/master.zip. Unpack it into the
    libraries  directory of your Drupal installation. 
    (Example: sites/all/libraries) Rename the folder 'side-comments', so that
    the full path would be sites/all/libraries/side-comments

 3. Enable the SideComments Now module in the administration tools.

 4. If you're not using Drupal's default administrative account, make sure
    "administer sidecomments now" is enabled through access control
    administration.

 5. Visit the SideComments Now settings page 
    (admin/config/content/sidecomments_now) and make appropriate configurations.
