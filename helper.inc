<?php
/**
 * @file
 * Helper functions for SideComments Now module.
 */

/**
 * Returns path of side-comments library.
 *
 * @return bool|string
 *   Path to the library.
 */
function _sidecomments_now_get_library_path() {

  $file = 'release/side-comments.js';
  $library = 'side-comments';

  // Implement simple cache.
  $library_path = &drupal_static(__FUNCTION__);
  if (!empty($library_path)) {
    return $library_path;
  }

  // Support libraries module.
  if (module_exists('libraries') && function_exists('libraries_get_path')) {
    $library_path = libraries_get_path($library) . "/$file";
    if (file_exists($library_path)) {
      return $library_path;
    }
  }
  else {
    $paths = array(
      'sites/all/libraries/' . $library,
      drupal_get_path('module', 'sidecomments_now') . '/' . $library,
      drupal_get_path('module', 'sidecomments_now') . "/libraries",
      'profiles/' . variable_get('install_profile', 'default') . '/libraries/' . $library,
    );
    foreach ($paths as $library_dir) {
      $library_path = $library_dir . "/$file";
      if (file_exists($library_path)) {
        return $library_path;
      }
    }
  }
  return FALSE;
}
