<?php

/**
 * @file
 * Administrative pages forms and functions for the SideComments Now module.
 */

/**
 * Form builder; Configure sidecomments_now settings for this site.
 *
 * @param array $form
 *   An associative array containing the structure of the form.
 * @param array $form_state
 *   An associative array containing the current state of the form.
 *
 * @return array
 *   The form structure.
 *
 * @see system_settings_form()
 */
function sidecomments_now_admin_settings(array $form, array &$form_state) {
  $form['sidecomments_now_compression'] = array(
    '#title' => t('SideComment.js compression level'),
    '#type' => 'radios',
    '#default_value' => variable_get('sidecomments_now_compression', SIDECOMMENTS_NOW_PRODUCTION),
    '#options' => array(
      SIDECOMMENTS_NOW_PRODUCTION => t('Production (minified)'),
      SIDECOMMENTS_NOW_DEVELOPMENT => t('Development (uncompressed)'),
    ),
    '#description' => t('Use Production unless you are actively developing.'),
  );

  $sidecomments_path = libraries_get_path('side-comments');
  $default_theme_css_path = $sidecomments_path . '/release/themes/default-theme.css';

  $module_path = drupal_get_path('module', 'sidecomments_now');

  $form['sidecomments_now_custom_theme_css'] = array(
    '#title' => t('Path to custom theme CSS'),
    '#type' => 'textfield',
    '#default_value' => variable_get('sidecomments_now_custom_theme_css', ''),
    '#description' => t('Provide a path to a custom sidecomment theme css file if you want to override the default theme. The default theme file is here: <a href="/@link_url">@link_url</a>.', array('@link_url' => $default_theme_css_path)),
    '#element_validate' => array('sidecomments_now_admin_settings_validate_theme_css'),
  );

  $form['sidecomments_now_disallowed_classnames'] = array(
    '#title' => t('Disallowed paragraph CSS classnames'),
    '#type' => 'textarea',
    '#description' => t('Paragraphs with these CSS classnames will not have sidecomment functionality. One classname per line.'),
    '#default_value' => variable_get('sidecomments_now_disallowed_classnames', SIDECOMMENTS_NOW_DISALLOWED_CLASSNAME),
    '#element_validate' => array('sidecomments_now_admin_settings_validate_disallowed_classnames'),
  );

  $form['sidecomments_now_callback_js'] = array(
    '#title' => t('Comment Attempted Callback Script'),
    '#type' => 'textarea',
    '#default_value' => variable_get('sidecomments_now_callback_js', sidecomments_now_callback_js_default()
    ),
    '#description' => t('Client-side JavaScript to be called when a user without commenting privileges attempts to submit a sidecomment. For example, you could input custom code here that would open a dialog with a login form. Do not include &lt;script&gt; tags.
    
    <p>Optionally, you can <strong>leave this field blank</strong> and provide your own callback function like this in a separate file:

    <pre>
    sideComments.on(\'addCommentAttempted\', function( comment ) {
      /* Your callback code. */
    });
    </pre>
        
    See <a href="https://aroc.github.io/side-comments-demo/">https://aroc.github.io/side-comments-demo/</a> for documentation on SideComments.js callbacks. See <a href="/!module_path/sidecomments_now.js">sidecomments_now.js</a> for a working example.
    ', array('!module_path' => $module_path)),
  );

  $form['sidecomments_now_comment_approval_message'] = array(
    '#title' => t('Comment Approval Message'),
    '#type' => 'textarea',
    '#default_value' => variable_get('sidecomments_now_comment_approval_message', t('Your comment has been queued for review by site administrators and will be published after approval.')),
    '#description' => t('Message displayed to users after a sidecomment is submitted to the comment approval queue.'),
  );

  if (module_exists('mollom')) {

    $form['mollom'] = array(
      '#title' => t('Mollom'),
      '#type' => 'fieldset',
    );
    $form['mollom']['sidecomments_now_mollom'] = array(
      '#title' => t('Use Mollom to moderate SideComments'),
      '#type' => 'checkbox',
      '#default_value' => variable_get('sidecomments_now_mollom', 0),
      '#description' => t('Note that you will still need to add the comment form for the content type you\'re working with in <a href="admin/config/content/mollom">Mollom administration</a>.'),
    );

    $form['mollom']['sidecomments_now_profanity_message'] = array(
      '#title' => t('Profanity Response Message'),
      '#type' => 'textarea',
      '#default_value' => variable_get('sidecomments_now_profanity_message', t('Your submission has triggered the profanity filter and will not be accepted.')),
      '#description' => t('Message displayed to users after a sidecomment is rejected as profane.'),
    );

    $form['mollom']['sidecomments_now_spam_message'] = array(
      '#title' => t('Spam Response Message'),
      '#type' => 'textarea',
      '#default_value' => variable_get('sidecomments_now_spam_message', t('Your submission has triggered the spam filter and will not be accepted.')),
      '#description' => t('Message displayed to users after a sidecomment is rejected as spam.'),
    );

  }

  $form = system_settings_form($form);

  return $form;

}

/**
 * Form element validation handler for custom theme css form element.
 *
 * @param array $element
 *   The structured array of the form element.
 * @param array $form_state
 *   An associative array containing the current state of the form.
 */
function sidecomments_now_admin_settings_validate_theme_css(array $element, array &$form_state) {
  $path = $element['#value'];
  if (!empty($path) && !file_exists($path)) {
    form_error($element, t('%path is not a valid path.', array('%path' => $path)));
  }
}

/**
 * Form element validation handler for disallowed classnames form element.
 *
 * @param array $element
 *   The structured array of the form element.
 * @param array $form_state
 *   An associative array containing the current state of the form.
 */
function sidecomments_now_admin_settings_validate_disallowed_classnames(array $element, array &$form_state) {
  $classnames = explode("\n", trim(check_plain($element['#value'])));
  foreach ($classnames as $classname) {
    if (trim($classname) == SIDECOMMENTS_NOW_ALLOWED_CLASSNAME) {
      form_error($element, t('%classname is reserved and cannot be used as a disallowed classname.', array('%classname' => SIDECOMMENTS_NOW_ALLOWED_CLASSNAME)));
      break;
    }
  }
}
