/**
 * @file
 * Custom JS for the SideComments Now module.
 */

(function ($) {
  'use strict';

  Drupal.behaviors.sidecomments_now = {
    attach: function (context) {

      var SideComments = window.require('side-comments');
      var currentUser = Drupal.settings.sidecomments_now.currentUser;
      var sideCommentNid = Drupal.settings.sidecomments_now.sideCommentNid;
      var existingComments = Drupal.settings.sidecomments_now.existingComments;
      var sideComments = new SideComments('body.comment-node-2 .field-name-body, body.comment-node-1 .field-name-body', currentUser, existingComments);
      var saveToken = Drupal.settings.sidecomments_now.saveToken;

      var hash = window.location.hash.substr(1);
      if (hash.search(/^comment-\d+$/) === 0) {
        var cid = hash.replace(/comment-/, '');
        var commentId = 'cid_' + cid;
        $("li[data-comment-id='" + commentId + "']").each(function () {
          $(this).parents('.side-comment').find('a.marker').click();
          $(this).get(0).scrollIntoView();
        });
      }

      if (typeof existingComments != 'undefined') {
        for (var i = 0; i < existingComments.length; i++) {
          var section = existingComments[i];
          for (var n = 0; n < section.comments.length; n++) {
            var comment = section.comments[n];
            if (parseInt(comment.status) === 0) {
              $("li[data-comment-id='" + comment.id + "']").addClass('comment-unpublished');
            }
          }
        }
      }

      sideComments.on('addCommentAttempted', function (comment) {
        window.addCommentAttemptedCallback();
      });

      sideComments.on('commentPosted', function (comment) {
        comment.nid = sideCommentNid;
        if (currentUser) {
          $.ajax({
            url: '/sidecomments_now/save',
            type: 'POST',
            data: {comment: comment, saveToken: saveToken},
            success: function (savedComment) {
              var msg = savedComment.response_message;
              $('.comment-msg').remove();
              if (savedComment.cid) {
                comment.id = 'cid_' + savedComment.cid;
                comment.comment = Drupal.checkPlain(comment.comment);
                sideComments.insertComment(comment);
                $('li[data-comment-id="' + comment.id + '"] .action-link').after('<div class="comment-msg">' + msg + '</div>');
              }
              else {
                $('.comment-form.active input.comment-box').after('<div class="comment-msg">' + msg + '</div>');
              }
            }
          });
        }
        else {
          // Do something if comment post is attempted but currentUser is not active.
        }
      });

      sideComments.on('commentDeleted', function (comment) {
        $('.comment-msg').remove();
        var cid = comment.id.replace(/cid_/, '');
        $.ajax({
          url: '/sidecomments_now/delete/' + cid,
          type: 'POST',
          data: {deleteToken: comment.deleteToken},
          success: function (success) {
            sideComments.removeComment(comment.sectionId, comment.id);
          }
        });
      });
    }
  };

})(jQuery);
