/**
 * @file
 * Custom admin-side JS for the SideComments Now module.
 */

(function ($) {
  'use strict';

  Drupal.behaviors.sideCommentsNowCT = {
    attach: function (context) {
      $('#edit-sidecomments-now-display').click(function (e) {
        if ($(this, context).is(':checked')) {
          $('#edit-comment-default-mode').attr('checked', false).attr('disabled', true);
          $('#edit-comment-subject-field').attr('checked', false).attr('disabled', true);
          $('#edit-comment-form-location').attr('checked', false).attr('disabled', true);
          $('#edit-comment-default-per-page').val(300);
          $('input[name=comment_preview]').val([0]);
          $('#edit-comment-preview input').attr('disabled', true);
        }
        else {
          $('#edit-comment-default-mode').attr('disabled', false);
          $('#edit-comment-subject-field').attr('disabled', false);
          $('#edit-comment-form-location').attr('disabled', false);
          $('#edit-comment-preview input').attr('disabled', false);
        }
      });
    }
  };

})(jQuery);
