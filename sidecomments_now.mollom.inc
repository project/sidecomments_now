<?php
/**
 * @file
 * Mollom-related functions for the SideComments Now module.
 */

/**
 * Mollom validation function. Called before saving a sidecomment.
 *
 * @param object $sidecomment
 *   The comment object.
 * @param object $node
 *   The node the comment belongs to.
 *
 * @return array|bool
 *
 *   A result from mollom()->checkContent() or FALSE if the form is not
 *   protected by Mollom.
 *
 * @see sidecomments_now_save()
 */
function sidecomments_now_mollom_validate($sidecomment, $node) {

  global $user;

  $forms = mollom_form_cache();
  $form_id = 'comment_node_' . $node->type . '_form';

  if (isset($forms['protected'][$form_id])) {

    $form_info = mollom_form_load($form_id);

    // Create valid params to send to mollom and validate.
    $params = array();
    $params['postBody'] = $sidecomment['comment'];
    $params['authorName'] = $user->name;
    $params['authorMail'] = $user->mail;
    $params['authorId'] = $user->uid;
    $params['authorCreated'] = $user->created;
    $params['authorIp'] = ip_address();
    $params['checks'] = $form_info['checks'];
    $params['strictness'] = $form_info['strictness'];
    $params['contextCreated'] = $node->created;

    $result = mollom()->checkContent($params);
    $result['form_info'] = $form_info;
    $result['params'] = $params;

    return $result;
  }

  return FALSE;
}


/**
 * Repurposed from mollom_validate_analysis().
 *
 * Mollom.module assumes that only forms will be used to validate input, so we
 * need to use our own validation function.
 *
 * @param array $result
 *   The result array from mollom()->checkContent().
 *
 * @return array
 *   The result array with messages and status values added.
 *
 * @see mollom_validate_analysis()
 */
function sidecomments_now_mollom_validate_analysis(array $result) {

  if (!$result) {
    return FALSE;
  }

  $form_info = $result['form_info'];
  $response_message = '';
  $comment_status    = COMMENT_NOT_PUBLISHED;
  $comment_validated = FALSE;

  // Trigger global fallback behavior if there is a unexpected result.
  if (!is_array($result) || !isset($result['id'])) {
    return _mollom_fallback();
  }

  // Prepare watchdog message teaser text.
  $teaser = '--';
  if (isset($result['postBody'])) {
    $teaser = truncate_utf8(strip_tags($result['postBody']), 40);
  }

  // Handle the profanity check result.
  if (isset($result['profanityScore']) && $result['profanityScore'] >= 0.5) {
    if ($form_info['discard']) {
      $response_message = check_plain(variable_get('sidecomments_now_profanity_message', t('Your submission has triggered the profanity filter and will not be accepted.')));
    }
    else {
      $comment_status = COMMENT_NOT_PUBLISHED;
    }
    mollom_log(array(
      'message' => 'Profanity: %teaser',
      'arguments' => array('%teaser' => $teaser),
    ));
  }
  elseif (isset($result['spamClassification'])) {

    $queued_message = variable_get('sidecomments_now_comment_approval_message', t('Your comment has been queued for review by site administrators and will be published after approval.'));

    switch ($result['spamClassification']) {
      case 'ham':
        if (user_access('skip comment approval')) {
          $comment_status = COMMENT_PUBLISHED;
          // Whee. Our comment is going to be published right away.
        }
        else {
          $response_message = $queued_message;
        }
        $comment_validated = TRUE;

        mollom_log(array(
          'message' => 'Ham: %teaser',
          'arguments' => array('%teaser' => $teaser),
        ), WATCHDOG_INFO);
        break;

      case 'spam':
        if ($form_info['discard']) {
          $response_message = check_plain(variable_get('sidecomments_now_spam_message', t('Your submission has triggered the spam filter and will not be accepted.')));
        }
        else {
          $comment_validated = TRUE;
        }
        mollom_log(array(
          'message' => 'Spam: %teaser',
          'arguments' => array('%teaser' => $teaser),
        ));
        break;

      case 'unsure':
        // There could be a case where we might want to show the captcha image,
        // so don't rule that out completely yet.
        $comment_validated = TRUE;
        $response_message = $queued_message;
        mollom_log(array(
          'message' => 'Unsure: %teaser',
          'arguments' => array('%teaser' => $teaser),
        ), WATCHDOG_INFO);
        break;

      case MOLLOM_ANALYSIS_UNKNOWN:
      default:
        // Mollom responded with a unknown spamClassification.
        // As there could be multiple reasons for this, it is not safe to
        // trigger the fallback mode.
        $comment_validated = TRUE;
        mollom_log(array(
          'message' => 'Unknown: %teaser',
          'arguments' => array('%teaser' => $teaser),
        ), WATCHDOG_ERROR);
        break;
    }
  }

  // A message to display to the user.
  $result['response_message'] = $response_message;
  watchdog('test', $response_message);
  // Should this comment be queued for moderation?
  $result['comment_status'] = $comment_status;
  // Should this comment be saved at all?
  $result['comment_validated'] = $comment_validated;

  return $result;
}
