DESCRIPTION
-----------

SideComments Now integrates SideComments.js and the Drupal core commenting
system, allowing users to add comments to content on a per-paragraph basis.

For documentation and more information about SideComments.js, visit
https://aroc.github.io/side-comments-demo/.


FEATURES
--------

 * Mollom Integration

   Mollom content moderation can be applied to sidecomment submission by
   activating the Mollom module and applying it to the comment form for the
   content type you're working with.

 * Customizable theme CSS

   Change the look and feel of the side comment interface by providing your own
   theme css file, replacing (not overriding) the default theme CSS file.

 * Customizable callback for users without commenting permissions

   What should happen when a user attempts to submit a comment but they don't
   have permissions to do so? You may want to display a login dialog or redirect
   to the login screen, for example.

 * Customizable feedback

   Customize the feedback that is returned when a comment is spam-filtered
   (using Mollom) or when it is submitted to the comment approval queue.

 * Granular control over paragraph commentability

   Prevent comments from being applied to any paragraph by giving it the
   "non-commentable-section" class. You can also define your own non-commentable
   classes.


REQUIREMENTS
------------

This module requires the following modules:

 * Comment (core)
 * simplehtmldom API (https://www.drupal.org/project/simplehtmldom)
 * Libraries API (https://www.drupal.org/project/libraries)

Sidecomments.js requires jQuery version 1.7 (minimum).


RECOMMENDED MODULES
-------------------

 * Mollom (https://www.drupal.org/project/mollom)
   When enabled, Mollom comment moderation can be applied to sidecomments.

 * jQuery Update (https://www.drupal.org/project/jquery_update)


INSTALLATION
------------

See the INSTALL.txt file.


SETUP
-----

To activate side comments for a content type:

 1. Go to the content type edit form (eg:
    admin/structure/types/manage/[content-type-name]). Click the "Use
    SideComments Now" checkbox in the "Comment settings" section and save.

 2. Existing nodes of this content type will need to be resaved. Each paragraph
    will be assigned an ID and the SideComments.js widget will be active next to
    each paragraph.

Comment administration works the usual way.


CONFIGURATION
-------------

 * Go to: admin/config/content/sidecomments_now

 * Paragraphs must be added to the commentable content at the time it is saved,
   not when it is rendered. Therefore you probably want to use a WYSIWYG editor.
   If you don't, you will need to add <p> tags manually and ensure that they are
   not altered by a text filter when the content is rendered.

 * You may also need to use a WYSIWYG editor and adjust its setting to
   allow data, class and id attributes. SideComments Now works by assigning
   these attributes to each paragraph in content when it is saved. Your WYSIWYG
   editor may strip those attributes, depending on your settings.
   
 * Ensure that your version of jQuery is 1.7 at a minimum.


KNOWN PROBLEMS AND LIMITATIONS
------------------------------

 * Currently only works on body fields.

 * The number of side comments displayed is limited by the setting on the
   content type edit screen. The default maximum 300, so if a node has more than
   300 comments, some comments will not be displayed. You could work around this
   by altering the content type edit screen and increasing the maximum, but this
   may have undesired consequences.

 * Any action that causes a paragraph to lose its assigned ID (eg, manually
   changing or removing the paragraph ID, uninstalling and reinstalling
   SideComments) can result in orphaned comments which will not be displayed.


MAINTAINERS
-----------

Current maintainers:
 * Dagwood Reeves (melvix) - https://www.drupal.org/u/melvix
